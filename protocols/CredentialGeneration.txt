title Credential Generation

Voter -> Pnyx: 'generate me a credential'
Pnyx -> Smart Contract: 'give me the election modulus'
Smart Contract -> Pnyx: m
Pnyx -> Pnyx: c = GenerateCredential(m)
Pnyx -> Smart Contract: Store c
Smart Contract -> Pnyx: h = txHash(c)
Pnyx -> Pnyx: s = Sign(h, privKey)
Pnyx -> Voter: (h, s)
