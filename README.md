# Purpose
This repository stores the architecture diagrams and source code that creates them.
# Usage
The tags in this repository use semantic versioning. The version number of the tag correlates to the version of the application that the architecture describes. For example tag `v1.0.0` is the architectural description of E-cclesia on Tezos version `1.0.0`.